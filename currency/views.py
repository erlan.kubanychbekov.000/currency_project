from django.http import JsonResponse
from .models import CurrentRequest

import requests


def get_current_usd(request):
    api_url = 'https://open.er-api.com/v6/latest/USD'

    last_current = CurrentRequest.objects.last()
    last_10_current = CurrentRequest.objects.all().order_by('-created_at')[:10]

    content = {
        'rate_now': None,
        'last_10_current': [{'rate': str(current.price), 'created_at': str(current.created_at)} for current in
                            last_10_current],
    }

    if not last_current or last_current.is_expired():
        try:
            response = requests.get(api_url)
            response.raise_for_status()
            rate = response.json()['rates']['RUB']
            content['rate_now'] = rate
            CurrentRequest.objects.create(price=rate)
        except requests.exceptions.RequestException as e:
            content['error'] = 'Failed to fetch data from API'
            return JsonResponse(content, status=500)
    else:
        content['rate_now'] = last_current.price

    return JsonResponse(content)
