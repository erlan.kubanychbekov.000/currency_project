from django.urls import path
from .views import get_current_usd

from django.views.generic.base import RedirectView

urlpatterns = [
    path('', RedirectView.as_view(url='/get-current-usd/')),
    path('get-current-usd/', get_current_usd, name='get-current-usd')
]
