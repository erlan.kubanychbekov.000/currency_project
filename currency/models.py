from django.db import models
from django.utils import timezone


class CurrentRequest(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    price = models.DecimalField(max_digits=10, decimal_places=2)

    def __str__(self):
        return f'rate:{self.price}'

    def is_expired(self):
        return (timezone.now() - self.created_at).total_seconds() > 10
